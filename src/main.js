// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Vodal from 'vodal'
import VueRouter from 'vue-router'
import routes from './routes'
import axios from 'axios'
import './js/topmenu.js'

Vue.use(VueRouter);

window.$http = axios;

// axios 기본 설정
axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.headers.common['Authorization'] = 'YORIBOGO';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.baseURL = 'http://prod.pj7wmuh3xm.ap-northeast-2.elasticbeanstalk.com/api/';
axios.defaults.withCredentials = true;
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'csrftoken';

/*
    axios csrftoken 설정, 임의로 csrftoken을 커스텀 헤더에 추가 함. 위 axios.defaults.xsrfHeaderName, xsrfCookieName으로는 추가가 안되는 이슈 때문에
    interceptor를 이용하여 필요한 헤더를 추가함
*/
axios.interceptors.response.use(function (response) {
    window.csrftoken = response.headers['x-csrftoken'];

    return response
});
axios.interceptors.request.use(function (request) {
    if (window.csrftoken) {
        request.headers['x-csrftoken'] = window.csrftoken;
    }

    return request
});

// IMPORT 모듈 초기화
IMP.init('imp75874419');

// Vue 비 부모-자식 컴포넌트 간 데이터 이동을 위한 eventBus
window.eventBus = new Vue({
    data: {
        cartItemNum: 0,
        productDetail: [],
        restItems: []
    },
    watch: {
        cartItemNum: function () {
            // @ receive target : headerInc.vue -> created HOOK
            this.$emit('numCount');
        }
    },
    created: function () {
        // @from : contentItem.vue -> transDetailPage()
        this.$on('productDetail', function (data) {
            this.productDetail = data;
            // reload시 detail Page content 유지시키기 위해 sessionStorage 사용
            window.sessionStorage.setItem('productDetail', JSON.stringify(data));
        })
    }
});

Vue.component(Vodal.name, Vodal);

Vue.config.productionTip = false;

var router = new VueRouter({
    mode: 'history',
    routes
});

/* eslint-disable no-new */
new Vue({
    components: {App},
    template: '<App/>',
    router
}).$mount('#app');
