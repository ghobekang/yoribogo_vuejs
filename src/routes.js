/**
 * Created by ghobe on 2018. 4. 25..
 */
import main from './components/mainContents.vue'
import join from './components/joinPage.vue'
import login from './components/loginPage.vue'
import cart from './components/cartPage.vue'
import cart_cart from './components/cartPage_sub/cartContent.vue'
import cart_coupon from './components/cartPage_sub/couponPage.vue'
import cart_editInfo from './components/cartPage_sub/editInfoPage.vue'
import cart_history from './components/cartPage_sub/orderHistoryPage.vue'
import cart_point from './components/cartPage_sub/pointPage.vue'

import sub_topmenuInner from './components/sub_topmenuInner.vue'
import contentDetail from './components/contentDetail.vue'

import order from './components/orderPage.vue'

export default [
    {
        path: '/',
        components: {
            default: main
        }
    },
    {
        path: '/join/',
        component: join
    },
    {
        path: '/login/',
        component: login
    },
    {
        path: '/cart/',
        component: cart,
        children: [
            {
                path: 'cart/',
                component: cart_cart
            },
            {
                path: 'coupon/',
                component: cart_coupon
            },
            {
                path: 'editInfo/',
                component: cart_editInfo
            },
            {
                path: 'orderHis/',
                component: cart_history
            },
            {
                path: 'points/',
                component: cart_point
            },

        ]
    },
    {
        path: '/submenuIn/',
        component: sub_topmenuInner,
        props: true
    },
    {
        path: '/detail/',
        component: contentDetail
    },
    {
        path: '/order/',
        component: order
    }
]