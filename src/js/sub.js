$(document).ready(function(){
    $('.detail-photo').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 800,
		autoplaySpeed: 4000,
		arrows: false,
		draggable: false,
		fade: true,
		dots: false,
		asNavFor: '.detail-thumnail',
	});
	$('.detail-thumnail').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		speed: 800,
		arrows: false,
		autoplaySpeed: 4000,
		asNavFor: '.detail-photo',
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		slide: 'div',
		responsive: [
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 3
			  }
			},
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 3
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 3
			  }
			}
		  ]
	});
   var buySel = false;
   $('.prodcut-ov .fr').find('.ex').click(function () {
	   if (buySel) {
			$('.buy-no-wrap').hide();
		   buySel = false;
	   } else {
		  $('.buy-no-wrap').show();
		   buySel = true;
	   }
   });
   var postView = false;
   $('.post-btn').click(function(){
	  if (postView) {
		   $(this).parent().parent().find('.post-layer').hide();   
		   postView = false;
	   } else {
		   
		   $(this).parent().parent().find('.post-layer').show();   
		    postView = true;
	   }
   });
   var orderView = false;
   //$('.order-ov, .order-info-wrap').click(function(){
	//  if (orderView) {
	//	   $('.order-detail-layer').hide();
	//	   orderView = false;
	//   } else {
	//
	//	   $('.order-detail-layer').show();
	//	    orderView = true;
	//   }
	//
   //});
	var material = false;
   $('.material-wrap').find('label').click(function(){
	  if (material) {
		   $(this).parent().parent().parent().find('.ctxt').hide();   
		   $(this).parent().parent().parent().find('.stxt').show();   
		   material = false;
	   } else {
		   
		   $(this).parent().parent().parent().find('.ctxt').show();  
		   $(this).parent().parent().parent().find('.stxt').hide();   
		    material = true;
	   }
	
   });
  var coubtin = false;
  $('.coupon-regist').click(function(){
	  if (coubtin) {
		$('.layer-pop.coupon').hide(); 
		   coubtin = false;
	   } else {
		   
		 $('.layer-pop.coupon').show();
		    coubtin = true;
	   }
	
   });
  var addrs = false;
  $('.addr-btn').click(function(){
	  if (coubtin) {
		$('.delivery-addr-wrap').hide(); 
		   coubtin = false;
	   } else {
		   
		 $('.delivery-addr-wrap').show();
		    coubtin = true;
	   }
   });
   $('.detail-info').find('.num-ctrl').each(function(){
		$(this).find('button[count_range]').click(function(e){
			e.preventDefault();
			var type = $(this).attr('count_range');
			var $count = $(this).parent().children('input.count');
			var count_val = $count.val(); // min 1
			if(type=='m'){
				if(count_val<1){
					return;
				}
				$count.val(parseInt(count_val)-1);
			}else if(type=='p'){
				$count.val(parseInt(count_val)+1);
			}
		});
   });
   $('.prodcut-ov > .fr > .btn-wrap').find('li').each(function(){
			$(this).find('.sw-btn').click(function(){
			$(this).addClass('active')
			$(this).parent().siblings('li').find('.sw-btn').removeClass('active')
			$(this).parent().siblings('li').find('.sr-btn').css({'display':'none'});
			$(this).parent().find('.sr-btn').css({'display':'block'});
			$(this).parent().siblings('li').find('.sr-btn').css({'display':'none'});
	   });
   });
});