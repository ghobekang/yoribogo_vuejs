
$(document).ready(function(){
	$('.cs-input').css({'color':'#e0d7c5'});
	$('.cs-input').change(function() {
	  var current = $('ts-input').val();
	  if (current != 'null') {
		  $(this).css({'color':'#47120e'});
	  } else {
		  $(this).css({'color':'#e0d7c5'});
	  }
	});
	
	$('.select-gray').css({'color':'#ccc'});
	$('.select-gray').change(function() {
	  var current = $('ts-input').val();
	  if (current != 'null') {
		  $(this).css({'color':'#333'});
	  } else {
		  $(this).css({'color':'#ccc'});
	  }
	});

   $('.top-popup').find('.close').click(function(){
		$('.top-popup').find('.top-slide').stop().animate({'height':0},400);
		$('.top-popup').stop().animate({'height':0},400);
   });
   var topshr_ = false;
   $('.top-search').find('.search-open').click(function () {
	   if (topshr_) {
		   $(this).parent('div').find('.is-search').hide();
		   ovHide();
		   topshr_ = false;
	   } else {
		   $(this).parent('div').find('.is-search').show();
		   ovShow();
		   topshr_ = true;

	   }
   });
   $('.top-search').find('.search-close').click(function () {
	   if (topshr_=true) {
		   $('.is-search').hide();
		   ovHide();
		   topshr_ = false;
	   } 
   });
	
	function ovShow(){
		var $cback = $(".search-slider-ovclick");
		$cback.unbind("click").bind("click",function(){ ovHide(); $('.is-search').hide();}).show();
	}
	function ovHide(){
		$(".search-slider-ovclick").hide();
		$('.is-search').hide()
		topshr_ = false;
	}


	$('.select-person').find('li >a ').click(function(){
		$(this).parent('li').addClass('active');
        $(this).parent('li').siblings('li').removeClass('active');
	});
});
