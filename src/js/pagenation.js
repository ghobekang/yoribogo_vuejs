/**
 * Created by ghobe on 2018. 7. 10..
 */
var pagination = function (arr, num) {
    var pageNum = arr.length / num
    var structuredArr = []

    function _init_Pagination () {
        for (var i = 0; i < pageNum; i++) {
            var sp = i * num, ep = ((i + 1) * num)
            structuredArr.push(arr.slice(sp, ep))
        }
    }
    function getTargetArr (pageNum) {
        return structuredArr[pageNum - 1]
    }

    _init_Pagination()

    return structuredArr
}

export default {
    pagination: pagination
}
