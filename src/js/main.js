$(document).ready(function(){
	// visual
	$('.mvisual-slide').slick({
		autoplay: true,
		dots: false,
		arrows: false,
		pauseOnDotsHover: false,
		pauseOnHover: false,
		speed: 300,
		autoplaySpeed: 3000,
		fade: true,
		adaptiveHeight: true,
		/*slidesToShow: 5,
        slidesToScroll: 1,*/
		infinite: true,
		cssEase: 'linear'
	});
	$('.visual-ctrl > .play').hide();
	$('.visual-ctrl > .stop').on('click', function() {
		$('.visual-ctrl > .play').show(); $(this).hide();
		$('.mvisual-slide').slick('slickPause');
	});

	$('.visual-ctrl > .play').on('click', function() {
		$('.visual-ctrl > .stop').show(); $(this).hide();
		$('.mvisual-slide').slick('slickPlay');
	});
	$('.visual-ctrl > .prev').on('click', function() {
		$('.mvisual-slide').slick('slickPrev');
	});

	$('.visual-ctrl > .next').on('click', function() {
		$('.mvisual-slide').slick('slickNext');
	});

});
function view_tab(f,b)	{
	for(i=1; i<=b; i++) {
		contentid = document.getElementById("v-con"+i);
		tabid = document.getElementById("v-tab"+i);
		if(i==f) {			
			contentid.style.display = "block";
			tabid.className = "active";
		}else{
			contentid.style.display = "none";
			tabid.className = "";
		}
	}
}